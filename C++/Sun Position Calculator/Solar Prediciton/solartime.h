#pragma once

#ifndef SOLARTIME_INCLUDE
#define SOLARTIME_INCLUDE


using namespace std;


class location {
private:

	std::string m_city;
	float m_longitude;
	float m_latitude;
	double m_panelTilt;
	double m_panelAzimuth;


public:

	location(std::string city, float longitude, float latitude);
	std::string getcity();
	float getlong();
	float getlat();
};


class solartime : public location
{

private:
	int m_month;
	int m_day;
	int m_hour;
	int m_d_since;
	int m_DST;

public:

	
	solartime(int month, int day, int hour, float latitude, float longitude, std::string city);
	solartime(int hour);
	~solartime();
	float solar_hour_angle();
	int days_since();
	float time_conversion(double time);

};



#endif
