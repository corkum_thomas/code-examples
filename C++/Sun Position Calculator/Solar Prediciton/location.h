#pragma once
#ifndef LOCATION_INCLUDE
#define LOCATION_INCLUDE

using namespace std;

class location {

private:
	float m_longitude;
	float m_latitude;

public:
	location(float longitude, float latitude);
	void print_location();
	friend class solartime;

	~location();

};

#endif 
