// Solar Prediciton.cpp : Defines the entry point for the console application.

// TODO:
// Create data structure with multiple cities and ask person for city
// Add panel tilt and calculate change in radiation per hour (Without Cloud Cover)
// Start predicting panel output based off panel tilt and solar location


#include "stdafx.h"
#include <iostream>
#include <math.h>
#include "solartime.h"


void main()
{
	float ott[2] = { 45.26, -75.6972 };  // [lat,long]
	solartime ottawa(1, 2, 12, ott[0], ott[1], "Ottawa");  //month, day, hour, lat, long, city
	ottawa.solar_hour_angle();

}

