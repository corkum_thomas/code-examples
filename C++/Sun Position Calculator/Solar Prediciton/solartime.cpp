#include "stdafx.h"
#include <iostream>
#include <math.h>
#include "solartime.h"


location::location(std::string city, float latitude, float longitude)
	:m_longitude(longitude), m_latitude(latitude), m_city(city)
		{}

float location::getlong()
	{return m_longitude;}

float location::getlat() 
	{return m_latitude;}

std::string location::getcity()
	{ return m_city; }


solartime::solartime(int month = 1, int day = 1, int hour = 1, float latitude = 1, float longitude = 1, std::string city = " ") 
	:m_month(month), m_day(day), m_hour(hour), location(city, latitude, longitude)
{
	m_d_since = days_since();
}

solartime::~solartime() {}

//Prints out all calculations for solar time to console
float solartime::solar_hour_angle() {

	//Finding Local Solar time (LST). [Doesn't account for day light savings] <- TODO
	//GMT Hard Coded for Ottawa <- TODO
	//Only goes to whole hours <- TODO

	int local_std_meridian_time;
	float B, solar_noon, eqn_of_time, hour_angle, time_corection_factor, local_solar_time, sunrise_time, sunset_time; // hour_angle;
	float solar_decl, solar_decl_deg, rising_hour_angle, rising_hr_angle_deg;
	int GMT = -5;  		
	
	float latitude = getlat();
	float longitude = getlong();
	
	local_std_meridian_time = 15 * GMT;								// Ottawa, Ontario is GMT/UTC - 5h during Standard Time - 4h during Daylight Saving Time

	B = 0.0172 * (m_d_since - 81); ;								 //0.986 = 360 / 365 (degrees of rotation/day) in Radians
	eqn_of_time = 9.87 * sin((2 * B)) - 7.53 * cos(B) - 1.58 * sin(B);
	time_corection_factor = 4 * ((longitude - local_std_meridian_time)) + eqn_of_time;	//LONGTITUDE = -75
	local_solar_time = m_hour + (time_corection_factor / 60);						//LST = LT + TC/60
	solar_noon = (12 - time_corection_factor / 60);	 
	cout << "solar noon" << time_conversion(solar_noon) << endl;
	solar_decl = (-0.409) * cos(0.0172 * (m_d_since + 10));					
	solar_decl_deg = solar_decl * 57.2958;												
	
	cout << "Solar Decl = " << solar_decl_deg << endl;
	
	hour_angle = 15 * (local_solar_time - 12);										
	
	cout << "Hour Angle" << hour_angle << endl;		//Hour angle in degrees commented for lack of current need
	
	rising_hour_angle = (acos(-tan((latitude * 0.01745)) * tan(solar_decl))) ;			//cout << "Rising hour angle = " << rising_hour_angle << endl;    //LATITUDE = 45
	rising_hr_angle_deg = rising_hour_angle * 57.2958;
 	sunset_time = (rising_hr_angle_deg / 15 + 12) - (time_corection_factor / 60);		 //convert sunrise angle to Local standard time
	sunrise_time = (-rising_hr_angle_deg / 15 + 12) - (time_corection_factor / 60);
	
	cout << "SunRise time = " << time_conversion(sunrise_time) << endl << "SunSet Time = " << time_conversion(sunset_time) << endl;
	cout << "Daylight Time = " << time_conversion(sunset_time - sunrise_time) << endl;
	
	return sunrise_time;

	//Calculating Hour Angle ( if(HRA == 0) {Solar Noon};  if(HRA < 0) {Morning}

}

//Calculates time since start of year to be used by solar_hour_angle calculations
int solartime::days_since() {

	//[Doesn't account for leap years] <-TODO

	int days_in_month[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	int month = m_month - 1;	//for array iteration
	int sum = -1;			    //First day = 0 days past, start day count at -1

	if (month > 12 || month < 0) {
		cout << "Month out of range" << endl;
		return 0;
	}
	else if (m_day > days_in_month[month] || m_day < 0) {
		cout << "Day out of range for corresponding Month" << endl;
		return 0;
	}
	else if (m_hour > 24 || m_hour < 0) {
		cout << "Hour Out of range" << endl;
	}

	for (int i = month; i >= 0; --i) {
		if (i == month) {
			sum += m_day;
		}
		else {
			sum += days_in_month[i];
		}
	}
	return sum;
}


float solartime::time_conversion(double time) {

	double fraction;
	double integer;

	fraction = modf(time, &integer);
	fraction *= 0.6;

	return integer + fraction; 
}






