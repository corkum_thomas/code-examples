/*///////////////////////////////////////////////////////
//Solar Explore C2000 Energy Monitor and Sepic Control
//For power control of Single Phase NanoGrid
//Thomas Corkum, 2016-2017
//Carleton University, Ottawa, Canada
////////////////////////////////////////////////////////

  ANALOG PIN SETUP
  A0 - PV Voltage
  A1 - Battery Voltage
  A6 - PV current
  A7 - Battery current
  
  DIGITAL PIN SET UP
  D7 - PWM OUT (200KHz)

*/

  #include <Wire.h> 
  #include <LiquidCrystal_I2C.h>
  #include <Arduino.h>
  

#define vboost 0
#define vbatt 1
#define iboost 2
#define ibatt 3


#define vboostpin 0
#define vbattpin 1
#define iboostpin 6
#define ibattpin 7

#define num_adcreadings 4

//ADC Reading Variables
float raw_adc[num_adcreadings]; //0 - 4095 ADC reading
float adc_array[num_adcreadings]; //Actual Levels
float adc_factors[num_adcreadings]= {10.091, 6.00, 1.25, 1};  //Voltage divider and Op-Amp factors
float vref = 3.300;
float resolution = 4095.00;
float adc_samples = 25.00;

//Duty Variables
int sepic_duty = 0.5;
float v_fwd = 0.486;

//Charge State Variables 
int chargestate = 0;   //0 = deep discharge,  1=Bulk, 2=Overcharge, Float charge = 3, 4 = discharge 
int previous_chargestate;
double stateofcharge;

//Battery Charging Voltage Variables 
float vbatt_v1 = 10.5; 
float vbatt_v2 = 13.4;
float vbatt_v3 = 14.4;
float vbatt_soc = 12;
float v_sepic_max = 16.0;

//Battery Charging Current Variables 
float ibatt_v1 = .240; 
float ibatt_ref;
float ibatt_setpoint;

//ACS 712 Current variables
float mVperAmp = 185;
float ACSoffset = 2287;

//Constant Variables for PI Controller

const float Kp = 0.06;
const float Ki = 0.003;
const float bulk_set_point = 0.5;
const float float_set_point = 13.4; //RESET
const float overcharge_set_point = 14.4; //RESET

//Global Variable For PI Controller
float i_temp = 0;


//LCD library 
LiquidCrystal_I2C lcd(0x27,20,4); 

//custom bits for LCD display
byte solar[8] = //icon for solar panel
{0b11111, 0b10101, 0b11111, 0b10101, 0b11111, 0b10101, 0b11111, 0b0000};
byte battery[8] =  //icon for battery
{0b01110, 0b11011, 0b10001, 0b10001, 0b10001, 0b10001, 0b10001, 0b11111};
byte load[8] =  // icon for power
{ 0b00010, 0b00100, 0b01000, 0b11111, 0b00010, 0b00100, 0b01000, 0b00000};
byte batterycharge[8] = //icon for battery charging
{ 0b01110, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111};


void setup()
{

  Serial.begin(9600);

////////////////////////////
//PWM FREQUENCY ADJUST 
////////////////////////////
//SEPIC_PWM: D7 (200KHZ signal) (Arduino DUE)

  int32_t mask_PWM_pin = digitalPinToBitMask(7);
  REG_PMC_PCER1 |= PMC_PCER1_PID36;                     // Enable PWM
  REG_PIOC_PDR |= mask_PWM_pin;                         // activate peripheral functions for pin (disables all PIO functionality)
  REG_PIOC_ABSR |= mask_PWM_pin;                        // choose peripheral option B
  REG_PWM_CLK = PWM_CLK_PREA(0) | PWM_CLK_DIVA(1);      // Set the PWM clock rate to 84MHz (84MHz/1)
  REG_PWM_CMR6 = PWM_CMR_CPRE_CLKA;                     // Enable single slope PWM and set the clock source as CLKA
  REG_PWM_CPRD6 = 420;                                  // Set the PWM frequency 84MHz/200kHz = 2100       
  REG_PWM_ENA |= PWM_ENA_CHID6;                         // Enable the PWM channel


//BUCK_BOOST_PWM: D6 (100KHZ signal) //Channel 7

  int32_t mask_BBG1PWM_pin = digitalPinToBitMask(6);
  REG_PIOC_PDR |= mask_BBG1PWM_pin;                   // activate peripheral functions for pin (disables all PIO functionality)
  REG_PIOC_ABSR |= mask_BBG1PWM_pin;                  // choose peripheral option 
  REG_PWM_CMR7 = PWM_CMR_CPRE_CLKA;                     // Enable single slope PWM and set the clock source as CLKA
  REG_PWM_CPRD7 = 840;                                  // Set the PWM frequency 84MHz/40kHz = 2100       
  REG_PWM_ENA |= PWM_ENA_CHID7;   

//BUCK_BOOST_PWM: D8 (100KHZ signal) //Channel 7

  int32_t mask_BBG2PWM_pin = digitalPinToBitMask(8);
  REG_PIOC_PDR |= mask_BBG2PWM_pin;                   // activate peripheral functions for pin (disables all PIO functionality)
  REG_PIOC_ABSR |= mask_BBG2PWM_pin;                  // choose peripheral option 
  REG_PWM_CMR7 = PWM_CMR_CPRE_CLKA;                     // Enable single slope PWM and set the clock source as CLKA
  REG_PWM_CPRD7 = 840;                                  // Set the PWM frequency 84MHz/40kHz = 2100       
  REG_PWM_ENA |= PWM_ENA_CHID7;   

//BUCK_BOOST_PWM: D9 (100KHZ signal) //Channel 7

  int32_t mask_BBG3PWM_pin = digitalPinToBitMask(9);
  REG_PIOC_PDR |= mask_BBG3PWM_pin;                   // activate peripheral functions for pin (disables all PIO functionality)
  REG_PIOC_ABSR |= mask_BBG3PWM_pin;                  // choose peripheral option 
  REG_PWM_CMR7 = PWM_CMR_CPRE_CLKA;                     // Enable single slope PWM and set the clock source as CLKA
  REG_PWM_CPRD7 = 840;                                  // Set the PWM frequency 84MHz/40kHz = 2100       
  REG_PWM_ENA |= PWM_ENA_CHID7;   
  
//D9 = CH4
//D8 = CH5
//D7 = CH6
//D6 = CH7
 
analogReadResolution(12);
  
////////////////////////////
//LCD SETUP
////////////////////////////

lcd.init();
lcd.backlight();

lcd.createChar(1, solar);
lcd.createChar(2, battery);
lcd.createChar(3, load);
lcd.createChar(4, batterycharge);


////////////////////////////////////////////////////////
//End Setup
////////////////////////////////////////////////////////
}


void loop() {


read_adc();                                   //Read and Convert ADC 
check_chargestate();                          //Check Charge State and S.O.C
PWM_Duty_Change();                           //PI controller 
REG_PWM_CDTY6 = (sepic_duty * 420);          //Set PWM Duty



//Print to LCD Display

lcd.setCursor(0,0);
lcd.print("Charge State");
lcd.setCursor(14,0);
lcd.print(chargestate);

lcd.setCursor(0,1);
lcd.print("State of Charge");
lcd.setCursor(16,1);
lcd.print(stateofcharge);
lcd.setCursor(19,1);
lcd.print("%");

lcd.setCursor(0,2);
lcd.print("Vbatt");
lcd.setCursor(6,2);
lcd.print(adc_array[vbatt]);



}


/////////////////////////////////////////
void read_adc(void)
{

//Read and Convert ADC pins

//Zero ADC array From Previous Reading 
  adc_array[num_adcreadings]={0};   
  
//Get ADC Readings

  for(int i = 0 ; i < adc_samples; i++)  
{
    raw_adc[vboost] += analogRead(vboostpin);
    raw_adc[vbatt] += analogRead(vbattpin);
    raw_adc[iboost] += analogRead(iboostpin);
    raw_adc[ibatt] += analogRead(ibattpin);
}

//ADC Conversion 

  for(int i = 0 ; i<=2; i++)  
 {
    raw_adc[i] /= adc_samples;
    raw_adc[i] *=  (vref / resolution);
    adc_array[i] = raw_adc[1] * adc_factors[i];
 }

//ACS712 Conversion

  raw_adc[ibatt] /= adc_samples;
  adc_array[ibatt] = ((raw_adc[ibatt] / resolution) * (vref * 1000.00));
  adc_array[ibatt] = ((adc_array[ibatt] - ACSoffset) / mVperAmp);

}



void check_chargestate(void)
{

//Determining charge state and Duty Cycles

//TODO: CHANGE TO SWITCH STATEMENT
// 	So inefficient with if statements...


// 0 = Deep Discharge, 1 = Bulk Charging, 2 = OverCharge, 3 = Float Charge, 4 = discharge

    previous_chargestate = chargestate;       //For signaling chargestate change

  if (adc_array[vbatt] < vbatt_v1)     //Deep Discharge
{
    chargestate = 0;
    sepic_duty = 0;
    stateofcharge = 0;
} 


   if (vbatt < vbatt_v3 && chargestate != 3)    //Bulk Charging (Constant Current)
{
    chargestate = 1;  
    sepic_duty = (v_sepic_max +v_fwd) / (adc_array[vboost] + v_sepic_max + v_fwd); 
    stateofcharge = ((vbatt_v3 - adc_array[vbatt]) / (vbatt_soc - vbatt_v3)) * 0.8;
}

   if (vbatt >= vbatt_v3 && adc_array[ibatt] >= ibatt_v1)           // Over Charge (Constant Voltage)
{
    chargestate = 2;
    sepic_duty = (vbatt_v3 + v_fwd) / (adc_array[vboost] + v_fwd + vbatt_v3);
    stateofcharge = 85;
    
} 
   if (vbatt >= vbatt_v3 && adc_array[ibatt] <= ibatt_v1)           //Float Charge (Constant Voltage)
{
    chargestate = 3;
    sepic_duty = (vbatt_v2 + v_fwd) / (adc_array[vboost] + v_fwd + vbatt_v3);
    stateofcharge = 95;
}
   if (vbatt >= (vbatt_v1 + 0.1) && adc_array[ibatt] <= 0)       //Discharge 
{
    chargestate = 4;
    sepic_duty = 0;
}

}

void PWM_Duty_Change(void)
{

     //Note: PI controller needs further Work


  //Local PI variables
  
  float imax = 100;
  float imin = -100;
  float Err_Value;
  float P_Term;
  float I_Term;
  float new_ADC_value;
  float set_point;
  int i;
  //Code

 //Using specific setpoint for each charge state 
if(chargestate == 1) 
  {set_point = bulk_set_point;
       i = 3;  }  
else if (chargestate == 2)
  {set_point = overcharge_set_point;
       i = 0;  }
else if (chargestate == 3)
  {set_point = float_set_point;
        i = 0; }  

  Err_Value = (set_point - raw_adc[i]); //Error value
  
  P_Term = Kp * Err_Value;
  i_temp += Err_Value;        

  //Preventing Intergal Windup
  if(i_temp > imax)
   { i_temp = imax; }
  else if (i_temp < imin)
   { i_temp = imin;}
 
  I_Term = Ki * i_temp;

  sepic_duty = sepic_duty + (P_Term + I_Term);
  
  //Limits on SEPIC Duty
  if (sepic_duty > 0.8)
    { sepic_duty = 0.8;}
  else if( sepic_duty < 0.1)
    { sepic_duty = .1;}
      
  }


