  var socket = io();
    socket.on('dirList', function(message){
      var dir = message.dirList; 						       	// Store File names in dir

      var fileSplit = dir.split('.');               // Parse file into parts
      var table = document.getElementById("directory");		// Link to Html table

      var row = table.insertRow(1);							// Insert New Row

      var file = row.insertCell(0);							// Get cell 0 in new row
      var fileType = row.insertCell(1);         // Get cell 1 in new row
      var action = row.insertCell(2);           // Get cell 2 in new row

      var link = document.createElement('a');   //Add href to new Element
      var href = '/fileServer/' + dir;
      file.innerHTML = fileSplit[0];        

      //If directory 
      if(fileSplit[1] == undefined){      

      	link.innerHTML = "Directory";
        action.innerHTML = 'open';
        link.href = '/fileServer/' + dir; 

        
      //If not directory 
      } else {

      	link.innerHTML = fileSplit[1];
        action.innerHTML = 'download';
        link.href = '/fileServer/' + dir;
        link.download = dir;

      }

       link.href = href;
       fileType.appendChild(link);

    });


