var http = require('http');          //Http requests
var fs = require('fs');              //File System
var express = require('express');    //Routing and App
var path = require('path');          //Dir paths
var ip = require("ip");              //Ip Address
var url = require('url');
var formidable = require('formidable');

var MongoClient = require('mongodb').MongoClient,
  assert = require('assert');

/* MongoDB code, requires DB hosted at :27017

MongoClient.connect('mongodb://localhost:27017/test', function(err, client){

  assert.equal(null, err);
  console.log("Mongo Connected");

  var db = client.db('test');

  db.collection('names').find({}).toArray(function(err,docs){
    console.log(docs);
  });
});
*/


var app = express();

var server = http.createServer(app);  //Create server with express
var port = 4000;                      //On port 4000

// Setup the View Engine 
app.set('view engine', 'html');
app.set('views', __dirname + '/node/views');
app.engine('.html', require('ejs').__express);


// Set the public static resource folder
app.use(express.static(path.join(__dirname, '/node/public')));

//Set up Server
var server = http.createServer(app);
server.listen(port, function(req, res){
	console.log('server is running at ' + ip.address() + ':' + port);
})

// Set up Socket
var io = require('socket.io')(server);

//Handle Routing of URL Requests
app.get('/*', function(req, res){

  var requested_path = url.parse(req.url, true);
  requested_path = requested_path.pathname;
  var requested_file = requested_path.split('/');

  //res.download (__dirname + '/' + 'documents' + '/' + 'ideas.txt');
  //res.end();

  if(requested_file[1] == 'fileServer'){
    if(requested_file[2]){
      var file = (__dirname + '/' + 'documents' + '/' + requested_file[2]) ;
      res.download(file);
    }
  }

  if (req.url == "/writing"){
   	 res.render('writing');

   } else if (req.url == "/homeAuto"){
   	 res.render('homeAuto');

   } else if ( req.url == "/fileServer"){
        res.render('fileServer');

   } else if (req.url == '/upload'){
      res.render('fileServer');
  
  } else if (req.url == '/test'){
      res.render('test');

   } else {
   	res.render('index');     // Everything else gets routed back to index.html
   }
});



// Socket for File Server 
io.on('connection', function(socket){
  fs.readdir( __dirname + '/Documents' , function(err, items){
      for (element in items){
        socket.emit('dirList', {'dirList': items[element]});
      }
  });
});


// Upload route.
app.post('/upload', function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files){
  
    //if (files.name == null){
      //return res.write('invalid upload')
    //}

    var oldpath = files.filetoupload.path;
    var newpath = __dirname + '/documents/' + files.filetoupload.name;
    fs.rename(oldpath, newpath, function(err){
      if (err) throw err;
      res.render('fileServer');
      res.end();
    });
  });
});






