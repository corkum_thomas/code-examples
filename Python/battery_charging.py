
# This module logs the current and voltage from two BK Precision meters.
# Controls a QPX600DP Dual 600w Power Supply for battery Charging
# Used For Auto Battery Cycling of Simpliphi 3.5kWh Battery Pack
# By Thomas Corkum
# 

# Discharging done with BK DC Electronic Load
# TODO: Add stop case for discharging.... Need Remotable Load or Controllable Switch

# Using 56V and 10A for charge point.
# Compensating for a 1V drop across the cables during charging. Real charge voltage = 55V


import sys
import serial
import datetime
import os
import time

def log():
    
# Connecting to Meters


    charge_state = input(" Charging (1) or discharging (2)? ")       # Determine Charge State by asking user
    
    try:
        current = serial.serial_for_url("COM6")
        current.timeout = 2
        current.write('*IDN?\n')
        current.readline()
        print "Using", current.readline().strip(), "for current."

        voltage = serial.serial_for_url("COM5")
        voltage.timeout = 2
        voltage.write('*IDN?\n')
        voltage.readline()
        print "Using", voltage.readline().strip(), "for voltage."
        voltage.write('FUNCtion VOLTage:DC\n')

        if charge_state == 1:
            power_supply = serial.serial_for_url("COM37")
            power_supply.timeout = 2
            power_supply.write('*IDN?\n')
            print "Using COM 37: ", power_supply.readline().strip(), "for Power Supply"
            power_supply.write('OP1 0\n')                                                  #Start with the power Supply off
        
    except:
        print "Meters were not found..."
        return


#Determining Charge state and type

    battery_serial = input("Insert Battery Serial Number: ")
    battery_serial = " B_Serial_" + str(battery_serial)
    
    if charge_state == 1:                                           # using 56.5V and 10A as charge point
        charge_type = input("Charge from unknown state (1) or charge from 0% (2)")
        if charge_type == 1:
            charge_type = str(' Charge Unknown to 100')     # Used For inital Battery Charge
        elif charge_type == 2:
            charge_type = str(' Charge 0 to 100')           # Used For Known Battery State Charge
            
        print "Using 56V and 10A charge"
        power_supply.write('V1 56\n')
        power_supply.write('I1 10\n')

    if charge_state == 2:
        charge_type = str(' Discharge from 100 to 0 ')

#Making Log File

    log_path = "Log Files\\"

    if not os.path.isdir(log_path):
        os.mkdir(log_path)

    timestamp = datetime.datetime.now().isoformat().split('T')
    date_folder = timestamp[0]
    folder_path = log_path + date_folder + "\\"

    if not os.path.isdir(folder_path):
        os.mkdir(folder_path)

    log_file = folder_path + timestamp[1][:8].replace(':','_') + charge_type + battery_serial + '.csv'
    log_txt = open(log_file, 'a+')
    log_txt.write('Time,Voltage,Current,AmpHrs,Watt,WattHrs\n')

            
#Initilization of Variables 

    start_time = time.time() # Time is in seconds
    new_time = start_time
    stop_time = 0
    step_time = 3

    #used for calculation of amp_hrs and W_hrs
    amp_min = 0; amp_hrs = 0; watt = 0; watt_min = 0; watt_hrs = 0
    prev_i = 0; prev_v = 0; 
    i_delta = 0; v_delta = 0
    I = 0; V = 0; 
    
    step_time_s = float(step_time)              
    step_time_m = step_time_s / 60
    step_time_h = step_time_m / 60
    running_time = 0

    
#Waiting For Prompt to start charging

    start_command = input(" Type 1 to start ")       # wait for user to prompt start
        if charge_state == 1:
            power_supply.write('OP1 1\n')
            print "charge cycle started"
            

    print "Waiting for the current to exceed 100 mA before starting."

    while True:
        current.write('FETCH?\n')
        current.readline() # Throw away the echo
        try:
            I = float(current.readline())
            if abs(I) > 0.1:
                break
        except:
            print "Got a bad reading."

#Starting Readings

    print "Starting the readings."
    while stop_time > time.time() or stop_time == 0:
        while new_time > time.time():
            None

        new_time = new_time + step_time
        running_time += step_time_h
        
        current.write('FETCH?\n')
        voltage.write('FETCH?\n')
        current.readline() # Throw away the echo
        voltage.readline() # Throw away the echo
        
        try:
            I = float(current.readline())
            V = float(voltage.readline())

            i_delta = (I + prev_i) / 2
            v_delta = (V + prev_v) / 2
                
            amp_min += i_delta * step_time_m    #Accumulate amp_hrs over time 
            amp_hrs += i_delta * step_time_h
            
            watt = i_delta * v_delta
            watt_min += watt * step_time_m
            watt_hrs += watt * step_time_h

            log_string = str(running_time) + ',' + str(V) + ',' + str(I) + ',' + str(amp_hrs)+ ',' + str(watt)+ ',' + str(watt_hrs)

            prev_i = I
            prev_v = V
            
        except:
            # Use the previous measurements
            current.flushInput()
            voltage.flushInput()
            log_string =  str(running_time) + ',' + str(V) + ',' + str(I) + ',' + str(amp_hrs)+ ',' + str(watt)+ ',' + str(watt_hrs)
            print "Got a bad reading. Using the previous values."

        
        if V >= 54.8 or V <= 10 and abs(I) < 0.1:      #If internal breaker trips, turn off power supply and stop readings
            print "End of Charge Cycle"
            if charge_state == 1:
                power_supply.write('OP1 0\n')
                print "Turning off Power Supply"
            break

        print "Elapsed time: " + str((time.time()-start_time)/60) + " min " + log_string
        log_txt.write(log_string + '\n')
        log_txt.flush()

    print "Finished readings"
    
    power_supply.write('OP1 0\n')
    power_supply.close()
    current.close()
    voltage.close()

    log_txt.close()














    
    
